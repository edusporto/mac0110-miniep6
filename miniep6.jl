# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function soma_pa(n, a_1, r)
    # n * (a_1 + a_n) / 2
    return n * (a_1 + (a_1 + (n-1)*r)) / 2
end

function soma_impares(primeiro_impar, n)
    return (primeiro_impar * n) + soma_pa(n, 0, 2)
end

function impares_consecutivos(n)
    val = n^3

    primeiro_impar = 1

    n == 1 && return 1

    while soma_impares(primeiro_impar, n) != val
        primeiro_impar += 2
    end

    return primeiro_impar
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    # formato: <m> <m^3> <impares consecutivos>

    print(m, " ")
    print(m^3, " ")

    impar = impares_consecutivos(m)

    for i in 1:m
        print(impar, " ")
        impar += 2
    end

    println()
end

function mostra_n(n)
    for i in 1:n
        imprime_impares_consecutivos(i)
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
# test()

